<?php
/**
 * Created by PhpStorm.
 * User: Mario Figueroa
 * Email: mfigueroa@tmwk.cl
 * Date: 16/06/2015
 */
class WhmcsVerifyLicense{

    private $license;
    private $localLicense;
    private $licenseFile = 'license.php';
    private $whmcsUrl;
    private $secretKey;
    private $localkeydays = 3;
    private $allowcheckfaildays = 5;
    private $result;

    /**
     * @return bool
     */
    public function verify()
    {
        if($this->verifyLocalLicense()){
            return true;
        }elseif($this->verifyRemoteLicense()){
            return true;
        }
        return false;
    }

    /**
     * @return bool
     */
    private function loadLocalLicense()
    {
        if(file_exists($this->licenseFile)){
            $license = '';
            include $this->licenseFile;
            $this->localLicense = $license;
            return true;
        }else{
            return false;
        }
    }

    /**
     * @return bool|mixed
     */
    public function verifyLocalLicense()
    {
        if ($this->loadLocalLicense()) {

            $localkey       = str_replace("\n", '', $this->localLicense);
            $localdata      = substr($localkey, 0, strlen($localkey) - 32);
            $md5hash        = substr($localkey, strlen($localkey) - 32);

            if ($md5hash == md5($localdata . $this->secretKey)) {
                $localdata          = strrev($localdata);
                $md5hash            = substr($localdata, 0, 32);
                $localdata          = substr($localdata, 32);
                $localdata          = base64_decode($localdata);
                $localkeyresults    = unserialize($localdata);
                $originalcheckdate  = $localkeyresults["checkdate"];
                if ($md5hash == md5($originalcheckdate . $this->secretKey)) {
                    $localexpiry = date("Ymd", mktime(0, 0, 0, date("m"), date("d") - $this->secretKey, date("Y")));
                    if ($originalcheckdate > $localexpiry) {
                        if(!$this->validateLocalInfo($localkeyresults))
                            return false;
                        else
                            return $localkeyresults;
                    }
                }
            }
        }
        return false;
    }

    /**
     * @param $result
     * @return bool
     */
    public function validateLocalInfo($result)
    {
        $result = (object)$result;

        if (!in_array($_SERVER['SERVER_NAME'], explode(",", $result->validdomain))) {
            $this->result->status = "Invalid";
            return false;
        }
        if (!in_array($_SERVER['SERVER_ADDR'], explode(",", $result->validip))) {
            $this->result->status = "Invalid";
            return false;
        }
        if ($result->validdirectory != dirname(__FILE__)) {
            $this->result->status = "Invalid";
            return false;
        }
        $this->result = $result;
        return true;
    }

    /**
     * @return array|object
     */
    public function verifyRemoteLicense()
    {
        $results = array();
        $checkdate = date("Ymd");
        $data = $this->getRemoteLicense();
        if ($data) {
            preg_match_all('/<(.*?)>([^<]+)<\/\\1>/i', $data, $matches);
            foreach ($matches[1] AS $k => $v) {
                $results[$v] = $matches[2][$k];
            }
        }

        if ($results["status"] == "Active") {
            $results["checkdate"] = $checkdate;
            $data_encoded = serialize($results);
            $data_encoded = base64_encode($data_encoded);
            $data_encoded = md5($checkdate . $this->secretKey) . $data_encoded;
            $data_encoded = strrev($data_encoded);
            $data_encoded = $data_encoded . md5($data_encoded . $this->secretKey);
            $data_encoded = wordwrap($data_encoded, 80, "\n", true);
            $results["localkey"] = $data_encoded;
            $this->createLocalLicense($data_encoded);
        }
        $results["remotecheck"] = true;
        $results = (object)$results;
        $this->result->remotecheck = true;
        $this->result = $results;
        return $results;
    }

    /**
     * @return mixed
     */
    public function getRemoteLicense()
    {
        $postfields["licensekey"] = $this->license;
        $postfields["domain"] = $_SERVER['SERVER_NAME'];
        $postfields["ip"] = $_SERVER['SERVER_ADDR'];
        $postfields["dir"] = dirname(__FILE__);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_URL, $this->whmcsUrl . "modules/servers/licensing/verify.php");
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $data = curl_exec($ch);
        curl_close($ch);
        return $data;
    }

    public function createLocalLicense($code)
    {
        $nuevoarchivo = fopen($this->licenseFile, "w+");
        fwrite($nuevoarchivo, '<?php $license = "'.$code.'";  ?>');
        fclose($nuevoarchivo);
    }

    /**
     * @param mixed $license
     */
    public function setLicense($license)
    {
        $this->license = $license;
    }

    /**
     * @param mixed $localLicense
     */
    public function setLocalLicense($localLicense)
    {
        $this->localLicense = $localLicense;
    }

    /**
     * @param mixed $whmcsUrl
     */
    public function setWhmcsUrl($whmcsUrl)
    {
        $this->whmcsUrl = $whmcsUrl;
    }

    /**
     * @param mixed $secretKey
     */
    public function setSecretKey($secretKey)
    {
        $this->secretKey = $secretKey;
    }

    /**
     * @param mixed $localkeydays
     */
    public function setLocalkeydays($localkeydays)
    {
        $this->localkeydays = $localkeydays;
    }

    /**
     * @param mixed $allowcheckfaildays
     */
    public function setAllowcheckfaildays($allowcheckfaildays)
    {
        $this->allowcheckfaildays = $allowcheckfaildays;
    }

    /**
     * @return mixed
     */
    public function getResult()
    {
        return $this->result;
    }
}